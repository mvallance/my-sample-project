# Installation guide process

Thank you for downloading this template from The Good Docs Project! Before using the template, read this document for best practices about how to research, write, and maintain this type of content. Want to explore more templates? Check them out in our templates GitLab repository.

## How to research an installation guide

The recommended steps for researching installation guides are:

1.	Define the topic of the document.
2.	Source existing documentation.
3.	Look for commonalities in installation guides.
4.	Source knowledgeable Technical Writers.
5.	Utilize AI tools such as ChatGPT
6.	Prepare a first draft of the installation guide

### 1.	Define the topic of the document
Define an installation guide and summarize why it is important. 
An example is: The installation guide covers all the steps a user needs to take to download and set up a project for further development or usage. The installation guide is an essential companion to software products as it provides step-by-step instructional how to install and configure software correctly.

### 2.	Source existing documentation
Identify sources of information related to an installation guide’s structure and content. Consider some projects as an initial case study; including OSS applications, CLIs, packages, libraries, and frameworks. Examples are:
•	GitHub CLI: https://github.com/cli/cli
•	Stripe Ruby SDK: https://github.com/stripe/stripe-ruby
•	Commerce Layer CLI: https://github.com/commercelayer/commercelayer-cli
•	OpenCV Python: https://github.com/opencv/opencv-python
•	Reactjs: https://reactjs.org/docs/getting-started.html
•	Nextjs: https://nextjs.org/docs
•	Netlify CLI: https://github.com/netlify/cli
•	Nodejs: https://github.com/nodejs/node
•	Python Firestore: https://github.com/googleapis/python-firestore
•	Vercel Commerce: https://github.com/vercel/commerce

### 3.	Look for commonalities in installation guides.
Highlight and summarize commonalties of installation guides of companies who create similar products. The findings can be tabulated to identify any recurring patterns within Installation Guides. Examples are:

| INSTALLATION GUIDE COMPONENTS | ADOBE | NVIDIA | MICROSOFT | GOOGLE |
|----------|----------|----------|----------|----------|
| Purpose of the guide.   | Outline of document. This document explains the following: bullet point list. | Explains the purpose of the document.  | Explains what the user will learn.  | Explains the purpose of the guide.    |
| Explain the product or service.   | Included in Outline above.     | Explains product.  | Included in Outline above.  | Included in Outline above.  |
| System requirements.   | Prerequisites before installation. Supported languages and browsers are listed.  | Prerequisites listed.  | Before you begin: numbered list with supporting statements.  | Before installation options provided.    |
| Installation procedure.   | Steps for installing are listed numerically. Start each step with a base verb. Images used. Notice of possible errors at particular steps.   | Steps for installing are listed numerically. Steps for uninstalling provided.  | Steps for installing are listed numerically. Start each step with a base verb. Images used.  | Steps for installing are listed numerically. Example code provided.     |
| Addtional resources.   | Installation videos. License agreement and restrictions of use.   | Legal notice, trademarks, and copyright information.  | Links to additional resources such as FAQs.    | Use of tabs for each language (C#, Java, Node.js).   |


### 4.	Source knowledgeable Technical Writers 
Requesting opinions from knowledgeable Technical Writers often result in valuable tips, recommendations and expectations users desire (that they often don’t get) from installation guide documentation. Also, books on Technical Writing are valuable resources. Examples are:
•	Docs for Developers: An Engineer’s Field Guide to Technical Writing. By Jared Bhatti et al. ISBN-13: 978-1484272169
•	Technical Writing Process: The simple, five-step guide that anyone can use to create technical documents such as user guides, manuals, and procedures. By Kieran Morgan. ISBN-13: 978-0994169310
•	Developing Quality Technical Information: A Handbook for Writers and Editors. By Michelle Carey et al. ISBN-13: 978-0133118971

### 5.	Utilize AI tools such as ChatGPT

Using AI tools like ChatGPT for researching commonalities in vast amounts of technical documents offers benefits such as rapidly identifying recurring patterns, extracting key insights efficiently and revealing trends across various texts. 
Writing a detailed prompt will result in more informative responses. An effective prompt requires clarity, context, specificity, completeness, instructions, clear language and examples. 

### 6.	Prepare a first draft of an installation guide. 
Share the first draft and obtain feedback from your community. Make the necessary edits and highlight in a second draft how you have addressed the feedback. 

## How to write Installation guide

The details of how to write an installation guide are contained in the Installation Guide Template Guide. To write an installation guide it is required you prepare the following sections:
- Introduction
- Installation types
- Overview
- System requirements
- Before you begin
- Installation steps
- Verify installation
- Post-installation”
- Configuration options
- Upgrade options
- Downgrade options
- Uninstallation options
- Troubleshooting
- Next steps
- Product version history
- Definition of terms
- Additional resources 

Some of these sections are optional; see the Installation Guide Template Guide.

## How to maintain Installation guide

To maintain an installation guide that remains relevant and helpful to its users over time, you should:
- keep the document up-to-date with the latest changes that may impact the installation process
- state the version
- check compatibility
- archive previous versions
- address common problems
- address user feedback and frequently asked questions
- check all external links are still active.


Explore other templates from The Good Docs Project. Use our feedback form to give feedback on this template.
