# Installation Guide Template Guide

This is the final TEMPLATE developed by BOLAJI & VALLANCE, June 2023. 

Read this document before you start working on the installation template.

## Introduction 

An Installation Guide covers all the steps necessary to install the product and set it up for further use.    

## Identify your audience

Generally, an Installation Guide is for users who have the sufficient technical expertise to understand the installation instructions provided. If your proposed audience requires some technical expertise to install the product, ensure to highlight and list these requirements in the prerequisites section of the document.

## Why do you need an Installation Guide?

An Installation Guide can:

* Help a user get started using your product. 
* Reduce the number of support requests related to installation issues.
* Establish consistency in the existing developer experience.
* Ensure increased retention of a user who explores your product. 
* Confirm a user has all they need to configure, customize, and/or upgrade your product as you ship new changes.
* Make sure a user knows where to find help.

## Installation types

If your product can be used in different environments, include a table that describes the different options so the reader can select the best one for their use. For example, include Main or Lite versions when a product has installable options with different functionalities, include the operating system such as Windows, Linux, and MacOS, or include cloud providers for products that require self-hosting to work such as CodeSpaces, CodeSandBox, and GitPod. Also, include a link to the appropriate heading within the Installation Guide for all the available options and add an introductory sentence to all tables.

{Installation types are listed in the following table:}

|  **Type** |  **Description**|  **More information** | 
 |----------|-----------------|-----------------------| 
|{Name of installation type} |{Description of installation type}| {Link to relevant documents}|
|  ... |  {Description of installation type} |  {Link to relevant documents} |


## Installation Guide vs. How-to Guides

Installation Guides are often confused with How-to Guides. It is essential to distinguish between an Installation Guide and a How-to Guide document.  

In summary, the difference is an Installation Guide shows how to install the product (as a procedure), while a "How-to" topic shows how to do something with the product after it's been installed. In detail, Installation Guides and How-to Guides appear similar in structure and appearance, provide step-by-step instructions for completing a task, consider including screen captures to illustrate the steps, and add lists to break down the instructions into manageable stages. However, the difference lies in the purpose of the instructions. An Installation Guide describes the process of setting up and configuring a specific software, application, or service, while a How-to Guide describes how to accomplish a particular task using a specific product or technology (which would already be installed). 

The key differences are listed in the following table:

|<strong>Installation Guide</strong>|<strong>How-to Guide</strong>|
|-|-|
|An Installation Guide provides specific instructions on how to install and set up a particular software product.|A How-to Guide takes the reader through a series of steps required to perform a specific task to solve a particular problem.|
|An Installation Guide is a prerequisite document a user reads before considering How-to Guides; because you need the product itself installed before you solve any problem related to that product. Due to the broad scope and availability of different variables and conditions in which an Installation Guide would function, an Installation Guide should be independent of a How-to Guide.| How-to Guide (or even a Tutorial) may include installation steps of the software or an application that needs to be installed as part of the How-to solution process. A hyperlink within the How-to Guide would link back to the dedicated Installation Guide.|

## Before writing an Installation Guide

Before you start working on your Installation Guide, identify the following: 



* The target user who will need to follow your Installation Guide. This will help determine the appropriate level of detail and terminology used. 
* The specific version of the software or application to be installed and the system requirements for installation. 
* Software or libraries that the existing software or application depends upon.
* Installation options such as following an Installation Wizard or customized installation. This will help you to provide the user with the appropriate level of information.
* Identify common issues that may arise during the installation process, and provide troubleshooting tips to help users overcome these issues.


## Where would you need an Installation Guide?



* Operating Systems, applications, plugins, and extensions. 
* SaaS (Software as a Service) applications.
* Open Source Software (OSS) product documentation.
* Hardware device product documentation.


## Steps required to write the Installation Guide

There are two categories of conditions in which an Installation Guide can exist: 



* Standalone: an independent Installation Guide document/page without other documentation. It includes all the necessary information required to install the software or application, including system requirements and step-by-step instructions.
* Integrated: an Installation Guide document/page integrated inside an existing README document.


## Writing the Installation Guide


### About the “Introduction” section

In this section, summarize the purpose of the Installation Guide and mention the benefits that will arise from this installation (e.g., increased performance, system stability, enhanced security).

{This is optional.}  Add a link to a demo of the installed product or a sandbox to try out the product.


### About the “Installation types” section

In this section, explain what is included within the Installation Guide. This can include a list of different versions to be installed as an option. Make sure you highlight the differences between the installation scenarios. These can be outlined in a table with columns indicating the name of the installation type, a summary of the installation type, and a link to the relevant installation steps within the Guide.


### About the “Overview” section

In this section, explain “what you get” with the installation (e.g., the command, major flags, command alias, plugins available, files downloaded, or application programs). Also, include a sequential end-to-end summary of the installation process that can serve as a quick link or reference section for users. A table with one column summarizing the specific process and a second column linking to a relevant document is recommended.

{This is optional.} If available, include links to previous versions; a table is recommended.


### About the “System requirements” section

In this section, the different installation types and subsequent requirements for each must be made clear. This section leads into the next section on specific prerequisites. Based on your use case, you can adjust the structure of this section by using it in reverse order, having the installation type as the heading and system requirements for that type as a sub-section. For example, “Install on Linux > System Requirements” and “Install on Windows > System Requirements”, instead of “System Requirements > Install on Linux > Install on Windows.”


### About the “Before you begin” section

In this section, explain the prerequisites. Prerequisites tell the user what they require to accomplish a goal. The Installation Guide prerequisites will include any dependencies or packages, such as installing a required version for your system. If specialist knowledge or skills are required, then list them as a prerequisite. 

As an option, you can include all the prerequisites or add links; a table is recommended. 


### About the “Installation steps” section

In this section, provide a short introduction to the step-by-step procedure based on the installation type. When writing this section:



* Use numeric steps.
* Categorize the steps into subheadings,  as required.
* Base the sub-categories on the depth of the installation.
* Add a one-sentence description of the step.
* Start each step with an active verb (e.g., open, download).
* Explain the expected result after completing each step.
* Include checks for success if each step is done correctly and/or tips if the installation didn’t work at each step.
* Mention installation options where required, but recommend one path.
* Add visuals (GIFs, images, or videos) where required.
* Add code block examples and snippets where required.


### About the “Verify installation” section

In this section, show how to confirm that the installation was successful.


### About the “Post-installation” section

In this section, provide an overview of options once the installation is completed. Also, account for anticipated problems during or after installation. Provide support/contact information for issue reports and feedback. Links to other relevant resources should be included if available. 


#### About the “Configuration options” section

In this section, provide information regarding post-installation configuration options. Describe the requirements for configuring the installed product for further usage. Provide links to other resources, if available. 


### About the “Upgrade options” section

In this section, provide information about upgrade (also known as an update)  options, if relevant. Describe how to install updates from a range of possible options. Provide a link to available updates with specific version numbers, release dates, and key features.


### About the “Downgrade options” section

In this section,  provide downgrade options, if supported.


### About the “Uninstallation options” section

In this section, clearly describe the procedure to uninstall the project. 


### About the “Troubleshooting” section

In this section, a number of anticipated problems and associated solutions should be listed. This section helps solve problems encountered during installation. Start with a problem statement, then indicate the cause and provide a solution. Additional information can be added (e.g., restart the computer). 

### About the "Next steps" section**

In this section, include essential or recommended steps to take after installing the product. Provide links to further resources, if available. Also, include support/contact information for issue reports and feedback.


### About the “Product version history” section

In this section, you can list previous versions in a table, providing the version number and whether the version was major, minor, or a patch release. Always ensure the change history is consistent across the documentation. Kindly refer to [semver.org]([https://semver.org](https://semver.org)) to learn about the semantic versioning specification.


### About the “Definition of terms” section

{This section is optional.} Provide a glossary table describing the terms, acronyms, and abbreviations used in the Installation Guide.


## Additional resources

{This section is optional.} Provide links to the Frequently Asked Questions (FAQ) section/document and Community Forums.